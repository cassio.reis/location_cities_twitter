from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter


geolocator = Nominatim()
filepath = 'name_file'  
with open(filepath) as fp:  
   line = fp.readline()
   cnt = 1
   while line:
       try:
       		line = fp.readline()
       		location=geolocator.geocode(line,timeout=15)
       		if location:
       			print(int(location.latitude),",",int(location.longitude))
       except ValueError as error_message:
       		print("Error: geocode failed on input %s with message %s"%(line, error_message.message))
