import numpy as np
import sys
import argparse
from math import sqrt, log10
#from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import subprocess



def amostra(number):
	read(number)


def read(number):
   
   file1="arquivo1.txt"
   file2="arquivo2.txt"
   file3="arquivo3.txt"
   f1 = open(str(number)+".file1_amostra.txt","w+") 
   f2 = open(str(number)+".file2_amostra.txt","w+") 
   f3 = open(str(number)+".file3_amostra.txt","w+") 
   _write(file1,f1,number)
   _write(file2,f2,number)
   _write(file3,f3,number)
   


def _write(file,f1,number):
   
	with open(file) as fp:  
		line = fp.readline()
		cnt = 0
		while line:
			line = fp.readline()
			cnt+=1
			if(cnt<=number):
				f1.write(line)
			else:
				break 




if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-1", "--one", help="Amostragem com 1000", action="store_true")
	parser.add_argument("-2", "--two", help="Amostragem com 5000", action="store_true")
	parser.add_argument("-3", "--three", help="--Amostragem com 15000", action="store_true")
	parser.add_argument("-4", "--four",help="--Amostragem com 30000", action="store_true")
	parser.add_argument("-5", "--five",help="--Amostragem com 45000", action="store_true")
	args = parser.parse_args()

	try:
		if args.one:
				amostra(1000)
				print ("Finalizado")		
		if args.two:
				amostra(5000)
				print ("Finalizado")		
		if args.three:
				amostra(15000)
				print ("Finalizado")		
		if args.four:
				amostra(30000)		
				print ("Finalizado")		
		if args.five:
				amostra(45000)	
				print ("Finalizado")		
	
	except Exception as e:

		print("Error: There's no file with the name '{}'".format(args.inputFile))
